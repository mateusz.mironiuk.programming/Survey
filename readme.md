## Overview
Repository contains program :
* PyPolling

Short description of PyPolling


## Requirements
PyPolling requires python 3.6 or greater.
In order to run programs you need to install packages from `requirements.txt`:
```
pip install -r requirements.txt
```

## Running PyPolling
In order to run the PyPolling:
```
python3 main.py questions.txt first_survey.csv
```

Example `questions.txt` file:
```
Question one?
Question two?
Question three?
```

`first_survey.csv` is the name of csv file containing historic data. This file will be continually updated with each completed survery.
