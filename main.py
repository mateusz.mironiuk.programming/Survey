import sys
from functools import reduce

import pandas as pd


class PyPolling:
    COL_NAMES = [
        'non resident', 'underaged', 'unemployed', 'imprisoned',
        'previous_survey'
    ]
    INPUTS_TO_ANSWERS = {
        'yes': 'T',
        'y': 'T',
        'no': 'F',
        'n': 'F',
        'skip': 'U',
        's': 'U',
        'quit': 'Q',
        'q': 'Q',
    }
    VALID_INPUTS = set(INPUTS_TO_ANSWERS.keys())

    def __init__(self, questions_path: str, data_path: str):
        self._data_path = data_path
        self._questions = self._read_questions(questions_path)
        self._survey_data = self._read_survey_data(data_path)
        self._success_rates = self._get_success_rates()

    def _read_survey_data(self, path: str) -> pd.DataFrame:
        df = pd.read_csv(path)
        for col_name in self.COL_NAMES:
            assert col_name in df.columns,\
                'Column {} not present in hisorical data'.format(col_name)
        return df

    @staticmethod
    def _read_questions(path) -> list:
        with open(path, 'r') as f:
            return [line.strip('\n') for line in f.readlines()]

    def _get_success_rates(self):
        if len(self._survey_data) == 0:
            return [1 for _ in self.COL_NAMES]
        rates = []
        samples_count = len(self._survey_data)
        if not samples_count:
            return [1 for _ in self.COL_NAMES]
        for column in self.COL_NAMES:
            counts = self._survey_data[column].value_counts()
            try:
                f_counts = counts.loc['F']
            except KeyError:
                f_counts = 0
            rates.append(f_counts / samples_count)
        return rates

    def _is_input_valid(self, user_input: str) -> bool:
        return user_input.lower() in self.VALID_INPUTS

    def _get_succes_rate(self):
        base_product = reduce(lambda x, y: x * y, self._success_rates)
        product_percentage = base_product * 100
        return int(round(product_percentage))

    def _get_answer(self):
        user_input = input('> ')
        while not self._is_input_valid(user_input):
            print('Incorrect answer format, try again')
            user_input = input('> ')
        answer = self.INPUTS_TO_ANSWERS[user_input.lower()]
        return answer

    def start(self):
        user_answers = {}
        print('\nWelcome in YouGov survey. Please answer following questions:')
        for num, (question, col_name) in enumerate(zip(self._questions, self.COL_NAMES)):
            print('\nYou have {}% chance to succeed!'.format(self._get_succes_rate()))
            print(question, '[Yes/No/Skip/Quit]')
            answer = self._get_answer()
            user_answers[col_name] = answer
            if answer == 'Q':
                print('Thank you for your time anyway!')
                sys.exit(0)
            elif not answer == 'U':
                self._success_rates[num] = 1 if answer == 'F' else 0
        if self._get_succes_rate() == 0:
            print('Thank you for the survey, unfortunately you did not qualify.')
        else:
            print('Congratulations! You have qualified!')
        next_idx = len(self._survey_data)
        for col_name, value in user_answers.items():
            self._survey_data.loc[next_idx, col_name] = value
        self._survey_data.to_csv(self._data_path, index=False)


if __name__ == '__main__':
    questions_path = sys.argv[1]
    data_path = sys.argv[2]
    PyPolling(questions_path, data_path).start()
