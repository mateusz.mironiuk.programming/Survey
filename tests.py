import unittest
from unittest.mock import patch

import pandas as pd

from main import PyPolling


class BasicTests(unittest.TestCase):
    def test_appending_historical_data(self):
        with unittest.mock.patch('builtins.input', return_value='n'):
            polling = PyPolling('questions.txt', 'test/test_appending_data.csv')
            len_before = len(polling._survey_data)
            polling.start()
            len_after = len(polling._survey_data)
            self.assertTrue(len_after, len_before + 1)
        df = pd.DataFrame(
            data=['F,T,F,F,F'.split(',')],
            columns='non resident,underaged,unemployed,imprisoned,previous_survey'.split(',')
        )
        df.to_csv('test/test_appending_data.csv', index=False)  # recreate df

    def test_empty_historical_data(self):
        with unittest.mock.patch('builtins.input', return_value='n'):
            polling = PyPolling('questions.txt', 'test/test_empty_data.csv')
            returning_value = polling.start()
            self.assertIsNone(returning_value)
            df = pd.DataFrame(
                data=[],
                columns='non resident,underaged,unemployed,imprisoned,previous_survey'.split(',')
            )
            df.to_csv('test/test_empty_data.csv', index=False)  # recreate df

    def test_quitting(self):
        with unittest.mock.patch('builtins.input', return_value='q'):
            polling = PyPolling('questions.txt', 'test/test_quitting.csv')
            with self.assertRaises(SystemExit) as cm:
                polling.start()
            self.assertEqual(cm.exception.code, 0)
        df = pd.DataFrame(
            data=['F,T,F,F,F'.split(','), 'F,U,F,F,F'.split(',')],
            columns='non resident,underaged,unemployed,imprisoned,previous_survey'.split(',')
        )
        df.to_csv('test/test_quitting.csv', index=False)  # recreate df


if __name__ == '__main__':
    unittest.main()
